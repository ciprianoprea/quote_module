<?php

/*
	Request helper to fetch all request data;
*/

use Carbon\Carbon;

if(!function_exists('request'))
{
	function request()
	{
		return (object) $_POST;
	}
}

if(!function_exists('dd'))
{
	function dd(...$params){
		print "<pre>";
		var_dump($params);
		print "</pre>";
		die;
	}
}