<?php

namespace QuoteApp\Repositories\Sales;

use QuoteApp\Models\Sales\QuoteModel;
use QuoteApp\Models\Sales\QuotePeriodModel;
use QuoteApp\Models\User;
use QuoteApp\Repositories\Product\Interfaces\ProductInterface;


class Quote {

	
	protected $products = [];
	protected $totalCost = 0;
	protected $userDetails = [];
	
	/**
	 * [$quote]
	 * @var [QuoteApp\Models\Sales\QuoteModel]
	 */
	protected $quote;

	/**
	 * [$user]
	 * @var [QuoteApp\Models\User]
	 */
	protected $user;

	/**
	 * @param ProductInterface
	 */
	public function addProduct(ProductInterface $product)
	{
		$this->products[] = $product;
		$this->totalCost += $product->cost();
	}

	/**
	 * @param array
	 */
	public function setUserDetails(array $details)
	{
		$this->userDetails = $details;
	}

	/**
	 * @return number
	 */
	public function getTotalCost()
	{
		return $this->totalCost;
	}

	/**
	 * @return array
	 */
	public function getProducts() : array
	{
		return $this->products;
	}

	/**
	 * @return array
	 */
	public function get() : array
	{
		return [
			'user_details' => $this->userDetails,
			'products' => array_map(function($item){
				return $item->get();
			},$this->products),
			'total' => $this->getTotalCost()
		];
	}

	/**
	 * Save quote in database
	 * @return boolean
	 */
	public function save() : bool
	{
		$this->setUser()->createQuote()->attachProducts();	

		return true;	
	}

	/**
	 * Save user in database
	 */
	public function setUser() : Quote
	{
		$this->user = User::firstOrCreate([
			'email' => $this->userDetails['email'],
		],
		[
			'name' => $this->userDetails['name'],
			'password' => password_hash($this->userDetails['password'],PASSWORD_BCRYPT),
			'phone' => $this->userDetails['phone']
		]);

		return $this;
	}

	public function getUser() : ?User
	{
		return $this->user;
	}

	/**
	 * Save quote in database
	 * @return Quote
	 */
	public function createQuote() : Quote
	{
		$this->quote = QuoteModel::create([
			'user_id' 		=> $this->user->id,
			'user_name' 	=> $this->user->name,
			'user_email' 	=> $this->user->email,
			'user_phone' 	=> $this->user->phone,
			'total' 		=> $this->getTotalCost()
		]);

		if(!$this->quote)
		{
			throw new \Exception("DB error");
		}

		return $this;
	}

	public function attachProducts() : Quote
	{
		$products = [];
		$productsWithPeriod = [];

		foreach ($this->products as $product) {

			$products[$product->getProductId()] = [

				'product_name' => $product->getName(),
				'quantity' => $product->getQuantity(),
				'unit_price' => $product->getPrice()
			];

			if($product->hasPeriod())
			{
				$productsWithPeriod[$product->getProductId()] = $product;
			}
			
		}

		$this->quote->products()->attach($products);

		$dbProducts = $this->quote->products()->whereIn('product_id',array_keys($productsWithPeriod))->get();

		foreach ($dbProducts as $key => $product) 
		{
			QuotePeriodModel::create([

				'start_date' => $productsWithPeriod[$product->id]->start_date(),
				'end_date' => $productsWithPeriod[$product->id]->end_date(),
				'quote_products_id' => $product->id,

			]);
		}

		return $this;
	}


}