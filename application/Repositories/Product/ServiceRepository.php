<?php

namespace QuoteApp\Repositories\Product;

use QuoteApp\Repositories\Product\ProductTimeLimited;
use QuoteApp\Repositories\Product\Product;
use QuoteApp\Repositories\Product\Interfaces\ProductInterface;
use Carbon\Carbon;

class ServiceRepository extends ProductTimeLimited implements ProductInterface
{

	use Product;

	/**
	 * @return number
	 */
	public function cost()
	{
		return $this->price * $this->countableHours();
	}

	/**
	 * @return integer
	 */
	protected function countableHours() : int
	{
		return $this->start_date->diffInHoursFiltered(function(Carbon $date) {

		        return !$date->isSunday() && ($date->hour >= 9 && $date->hour < 19);

		}, $this->end_date);
	}

	/**
	 * @return array
	 */
	public function get() : array
	{
		return [
			'name' => $this->name,
			'type' => 'Service',
			'hourlyPrice' => $this->price,
			'quantity' => $this->countableHours(),
			'subtotal' => $this->cost(),
		];
	}

	/**
	 * @return integer
	 */
	public function getQuantity() : int
	{
		return $this->countableHours();

	}
}