<?php

namespace QuoteApp\Repositories\Product;

use QuoteApp\Repositories\Product\Product;
use QuoteApp\Repositories\Product\Interfaces\ProductInterface;

class GoodRepository implements ProductInterface
{
	use Product;

	protected $qty = 0;

	/**
	 * @return [type]
	 */
	public function cost()
	{
		return $this->price * $this->qty;
	}

	/**
	 * @param  integer $qty
	 * @return ProductInterface instance
	 */
	public function quantity($qty) : ProductInterface
	{
		$this->qty = $qty ?: 1;

		return $this;
	}

	/**
	 * @return array
	 */
	public function get() : array
	{
		return [
			'name' => $this->name,
			'type' => 'Good',
			'price' => $this->price,
			'quantity' => $this->qty,
			'subtotal' => $this->cost(),
		];
	}

	/**
	 * @return integer
	 */
	public function getQuantity() : int
	{
		return $this->qty;

	}
}