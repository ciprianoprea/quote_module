<?php

namespace QuoteApp\Repositories\Product;

use QuoteApp\Repositories\Product\ProductTimeLimited;
use QuoteApp\Repositories\Product\Product;
use QuoteApp\Repositories\Product\Interfaces\ProductInterface;
use Carbon\Carbon;

class SubscriptionRepository extends ProductTimeLimited implements ProductInterface
{
	use Product;
	
	/**
	 * @return number
	 */
	public function cost()
	{
		return $this->price * $this->countableDays();
	}

	/**
	 * @return integer
	 */
	protected function countableDays() : int
	{
		return $this->start_date->diffInDaysFiltered(function(Carbon $date) {
		        return !$date->isWeekend();
		}, $this->end_date);
	}

	/**
	 * @return array
	 */
	public function get() : array
	{
		return [
			'name' => $this->name,
			'type' => 'Subscription',
			'dailyPrice' => $this->price,
			'quantity' => $this->countableDays(),
			'subtotal' => $this->cost(),
		];
	}

	/**
	 * @return integer
	 */
	public function getQuantity() : int
	{
		return $this->countableDays();

	}
}