<?php

namespace QuoteApp\Repositories\Product;

use QuoteApp\Repositories\Product\Interfaces\ProductInterface;
use Carbon\Carbon;

trait Product
{
	protected $price = 0;
	protected $name;
	protected $product_id;

	/**
	 * @param number $price
	 */
	public function setPrice($price) : ProductInterface
	{
		$this->price = $price;

		return $this;
	}

	/**
	 * @param string $name
	 */
	public function setName($name) : ProductInterface
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * @param int $product_id
	 */
	public function setProductId($product_id) : ProductInterface
	{
		$this->product_id = $product_id;

		return $this;
	}

	public function getPrice()
	{
		return $this->price;
	}

	public function getName()
	{
		return $this->name;
	}

	public function getProductId()
	{
		return $this->product_id;
	}

	/**
	 * @return boolean
	 */
	public function hasPeriod() : bool
	{
		return $this->hasPeriod ?? false;
	}
	
}

