<?php

namespace QuoteApp\Repositories\Product\Interfaces;

interface ProductInterface {

	/**
	 * @return number
	 */
	public function cost();

	/**
	 * @param number $price
	 */
	public function setName($price) : ProductInterface;
	/**
	 * @param integer $id
	 */
	public function setProductId($id) : ProductInterface;

}