<?php

namespace QuoteApp\Repositories\Product;

use Carbon\Carbon;

/**
 * This class is inherited by products 
 */

abstract class ProductTimeLimited 
{
	/**
	 * @var string
	 */
	protected $start_date;
	/**
	 * @var string
	 */
	protected $end_date;
	/**
	 * @var [type]
	 */
	protected $hasPeriod = true;

	/**
	 * @param  string
	 * @param  string
	 * @return object
	 */
	public function interval($start_date, $end_date) : object
	{
		$this->start_date = Carbon::parse($start_date);
		$this->end_date = Carbon::parse($end_date);

		if ($this->start_date > $this->end_date) {
			
			throw new \Exception("Start date is greater than end date");
		}

		return $this;
	}

	public function start_date()
	{
		return $this->start_date;
	}

	public function end_date()
	{
		return $this->end_date;
	}

}

