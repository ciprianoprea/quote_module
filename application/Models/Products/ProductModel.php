<?php

namespace QuoteApp\Models\Products;

use Illuminate\Database\Eloquent\Model as Eloquent;

class ProductModel extends Eloquent

{

   /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  
  protected $table = "products";

   protected $fillable = [

       'name', 'price','type'

   ];

 }