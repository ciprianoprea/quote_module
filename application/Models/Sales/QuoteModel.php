<?php

namespace QuoteApp\Models\Sales;

use Illuminate\Database\Eloquent\Model as Eloquent;
use QuoteApp\Models\Products\ProductModel;
use QuoteApp\Models\Sales\QuotePeriodModel;

class QuoteModel extends Eloquent

{

  protected $table = "quotes";
   /**
   * The attributes that are mass assignable.
   *
   * @var array
   */

   protected $fillable = [

       'user_id', 'user_name', 'user_email','user_phone','total'

   ];

   
   public function products()

   {
       return $this->belongsToMany(ProductModel::class,'quote_products','quote_id','product_id')->withPivot('product_name','quantity','unit_price');

   }

 }