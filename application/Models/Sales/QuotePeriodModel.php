<?php

namespace QuoteApp\Models\Sales;

use Illuminate\Database\Eloquent\Model as Eloquent;

class QuotePeriodModel extends Eloquent

{

  protected $table = "quote_products_period";

   /**
   * The attributes that are mass assignable.
   *
   * @var array
   */

   protected $fillable = [

       'quote_products_id', 'start_date', 'end_date'

   ];

   /*
   * Get Quote of Period
   *
   */

   public function quote()

   {
       return $this->belongsTo(QuoteModel::class,'quote_products_id');

   }

 }