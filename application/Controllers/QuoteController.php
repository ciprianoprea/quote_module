<?php

namespace QuoteApp\Controllers;

use QuoteApp\Http\ApiResponse;
use QuoteApp\Models\Products\ProductModel;
use QuoteApp\Repositories\Product\GoodRepository;
use QuoteApp\Repositories\Product\ServiceRepository;
use QuoteApp\Repositories\Product\SubscriptionRepository;
use QuoteApp\Repositories\Sales\Quote;

/**
 * QuoteController manage the http request for quote
 */
class QuoteController
{
	/**
	 * @return view
	 */
	public function create()
	{
		$products = ProductModel::get();

		return require '../views/quotes/create.php';
	}
	
	/**
	 * @return json
	 */
	
	public function store()
	{
		try {

			$quote = new Quote();

			$this->validateRequest();

			$quote->setUserDetails(request()->user_details);

			$products = ProductModel::whereIn('id',array_column(request()->products,'id'))->get();

			$products = array_map(function($product) use ($products) {

				$dbProduct = $products->where('id',$product['id'])->first();
				$product['name'] = $dbProduct->name;
				$product['price'] = $dbProduct->price;
				$product['type'] = $dbProduct->type;

				return $product;

			},request()->products);

			$products = (object) $products;

			foreach ($products as $key => $product) {

				$product = (object) $product;
				switch ($product->type) {
					case 'good':
						$quote->addProduct(
							(new GoodRepository())
							->quantity($product->qty)
							->setPrice($product->price)
							->setName($product->name)
							->setProductId($product->id)
						);
						break;

					case 'service':
						$quote->addProduct(
							(new ServiceRepository())
							->interval($product->start_date,$product->end_date)
							->setPrice($product->price)
							->setName($product->name)
							->setProductId($product->id)
						);
						break;

					case 'subscription':
						$quote->addProduct(
							(new SubscriptionRepository())
							->interval($product->start_date,$product->end_date)
							->setPrice($product->price)
							->setName($product->name)
							->setProductId($product->id)
						);
						break;
					
					default:
						break;
				}
			}

			$quote->save();
			
		} catch (\Exception $e) {
			
			return ApiResponse::setStatus(500)->errorResponse($e->getMessage());
		}

		return ApiResponse::successResponse($quote->get());
	}

	/**
	 * @return void
	 */
	
	protected function validateRequest() : void
	{
		if(!count(request()->products ?? []))
		{
			throw new \Exception("Products missing");
			
		}

		foreach (request()->user_details as $key => $value) 
		{
			if(!$value)
			{
				throw new \Exception("User {$key} is required");				
			}

			if($key == 'email' && !filter_var($value, FILTER_VALIDATE_EMAIL))
			{
				throw new \Exception("Invalid {$key}");
			}

			if($key == 'phone' && preg_match('/d{8,10}/', $value))
			{
				throw new \Exception("Invalid {$key}");
			}

			if($key == 'password' && strlen($value) < 6)
			{
				throw new \Exception("Password is too short");
			}
		}
	}
}