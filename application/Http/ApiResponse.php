<?php

namespace QuoteApp\Http;

class ApiResponse
{
	/**
	 * [$status description]
	 * @var integer
	 */
	protected static $status = 200;


	/**
	 * @param int
	 */
	public static function setStatus(int $statusCode)
	{
		$response = new static;
		
		$response::$status = $statusCode;

		return $response;
	}


	/**
	 * @param  $data
	 * @return json
	 */
	public static function successResponse($data)
	{

		$response =  [
			'data' => $data,
			'status' => self::$status
		];

		header('Content-Type: application/json');
		http_response_code(self::$status);

		return json_encode($response);
	}

	
	/**
	 * @param  string
	 * @return json
	 */
	public static function errorResponse(string $message)
	{

		$response =  [
			'message' => $message,
			'status' => self::$status
		];

		header('Content-Type: application/json');
		http_response_code(self::$status);
		
		return json_encode($response);
	}


}