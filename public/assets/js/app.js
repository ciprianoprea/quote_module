var quote = new Vue({
	el:'#quote',
	data : {
		quote : {
			user_details : {
				name : '',
				password : '',
				phone : '',
				email : '',
			},
			products : []
		},
		products : products,
		step : 1,
		quote_saved : {},
		error : {},
	},
	methods : {

		unit(type)
		{
			let units = {
				'good' : 'buc',
				'service' : 'hour',
				'subscription' : 'day',
			}

			return units[type];
		},
		next()
		{
			if(this.isValidStep())
			{
				this.step++;
			}
		},
		isValidStep()
		{
			if(this.step == 1)
			{
				return (this.quote.user_details.name && this.quote.user_details.password && this.quote.user_details.phone && this.quote.user_details.email)
			}
			else if(this.step == 2)
			{
				return this.quote.products.length
			}
		},
		addProduct(product,index)
		{
			if(product.qty)
			{
				this.quote.products[index] = {
					'id' : product.id,
					'qty' : product.qty,
					'name' : product.name,
				};

			}
			else
			{
				this.quote.products[index] = {
					'id' : product.id,
					'start_date' : product.start_date,
					'end_date' : product.start_date,
					'name' : product.name,
				}
			}

			this.$forceUpdate()
			
		},

		save()
		{
			axios.post('/quote/store',this.quote)
		    .then((response) => {
			    	this.quote_saved = response.data.data
			    	this.step++;
		    	}
		    )
		    .catch(e => {
		    	this.error = e.response.data.message;
		    	this.step = 1;
		    })

			
		}
	}
});