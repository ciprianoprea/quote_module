<?php 

use QuoteApp\Controllers\QuoteController;

$data = file_get_contents("php://input");
$data = json_decode($data, TRUE);

if($data)
{
	$_POST = $data;
}

require '../bootstrap.php';
require '../application/helpers.php';

$controller  = new QuoteController();

if(count($_POST))
{
	print $controller->store();
	return;
}

print $controller->create();



