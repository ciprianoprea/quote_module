<?php


require 'bootstrap.php';

use Illuminate\Database\Capsule\Manager as DB;


if (!DB::schema()->hasTable('users'))
{
	DB::schema()->create('users', function ($table) {

       $table->increments('id');

       $table->string('name');
       $table->string('password');
       $table->string('email');
       $table->string('phone');
       $table->timestamps();

   });
}

if (!DB::schema()->hasTable('quotes'))
{
	DB::schema()->create('quotes', function ($table) {

       $table->increments('id');

       $table->integer('user_id');
       $table->string('user_name');
       $table->string('user_email');
       $table->string('user_phone');
       $table->float('total',8,2);
       $table->timestamps();

       $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

   });
}

if (!DB::schema()->hasTable('products'))
{
  DB::schema()->create('products', function ($table) {

       $table->increments('id');
       $table->string('name');
       $table->float('price',8,2);
       $table->string('type');
       $table->timestamps();

  });

  \QuoteApp\Models\Products\ProductModel::insert([

        [
          'type' => 'good',
          'price' => 25,
          'name' => 'PHP book',
        ],
        [
          'type' => 'service',
          'name' => 'Extra teaching PHP',
          'price' => 2
        ],
        [
          'type' => 'subscription',
          'name' => 'Learn PHP',
          'price' => 5
        ]

  ]);
}


if (!DB::schema()->hasTable('quote_products'))
{
	DB::schema()->create('quote_products', function ($table) {

       $table->increments('id');

       $table->integer('quote_id');
       $table->integer('product_id');
       $table->string('product_name');
       $table->string('quantity');
       $table->string('unit_price');
       $table->timestamps();

       $table->foreign('quote_id')->references('id')->on('quotes')->onDelete('cascade');

   });
}

if (!DB::schema()->hasTable('quote_products_period'))
{
	DB::schema()->create('quote_products_period', function ($table) {

       $table->increments('id');
       $table->integer('quote_products_id');
       $table->timestamp('start_date');
       $table->timestamp('end_date');
       $table->timestamps();

       $table->foreign('quote_products_id')->references('id')->on('quote_products')->onDelete('cascade');

   });
}

print 'Database has been set';