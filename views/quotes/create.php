<!DOCTYPE html>
<html>
<head>
	<title>Create new quote</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>

	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<div class="container">
			<h1>Welcome to my app</h1>
		</div>
	</nav>

	<div id = "quote" class="container">
		
		
		<br>
			<h2>Step <span v-text = "step"></span></h2>
			<div class = "step1 row" v-if ="step == 1">
				<div class="form-group col-md-6">
					<label>Name</label>
					<input required type="text" name="" class = "form-control" v-model = "quote.user_details.name"/>
				</div>
				<div class="form-group col-md-6">
					<label>Password</label>
					<input required placeholder = "Min 6" type="password" name="" class = "form-control" v-model = "quote.user_details.password"/>
				</div>
				<div class="form-group col-md-6">
					<label>Phone</label>
					<input required placeholder = "Min 8 digits" type="number" name="" class = "form-control" v-model = "quote.user_details.phone"/>
				</div>
				<div class="form-group col-md-6">
					<label>E-mail</label>
					<input required placeholder = "your.email@example.com" type="email" name="" class = "form-control" v-model = "quote.user_details.email"/>
				</div>
			</div>
			<br>
			<div class = "row step2" v-if ="step==2">

				<div class = "col-md-4" v-for = "(product,index) in products" >
					<div class="card col-md-12" style="min-height: 400px">
						<h2 v-text = "product.name"></h2>
						<p >Price: $<span v-text = "product.price"></span>/<span v-text= "unit(product.type)"></span>
						</p>
						
						<div v-if = "product.type != 'good' ">
							<div class="form-group">
								<label>Start date</label>
								<input type="date" name = "start_date" class="form-control" v-model = "product.start_date">
							</div>
							<div class="form-group">
								<label>End date</label>
								<input type="date" name = "end_date" class="form-control" v-model = "product.end_date">
							</div>
							
						</div>
						<div v-else>
							<label>Quantity:</label>
							<input type="number" name = "qty" min = "1" class="form-control" v-model = "product.qty">
						</div>
						<br>
						<div class="text-center">
							<button type = "button" class ="btn btn-success" @click = "addProduct(product,index)" style="position: absolute;bottom: 10px;width: 90%;left: 5%">Add to quote</button>
						</div>
						<br>
					</div>
				</div>
			</div>

			<div class = "step3" v-if ="step==3">
				<h3>Quote summary</h3>
				<table class = "table">
					<tr>
						<td></td>
						<td>Name</td>
						<td>Quantity</td>
						<td>Start date</td>
						<td>End date</td>
					</tr>

					<tr v-for = "(product,index) in quote.products">
						<td>
							{{ index + 1 }}
						</td>
						<td>
							{{ product.name }}
						</td>
						<td>
							{{ product.qty ? product.qty : '-'  }}
						</td>
						<td>
							{{ product.start_date ? product.start_date : '-'  }}
						</td>
						<td>
							{{ product.end_date ? product.end_date : '-'  }}
						</td>
					</tr>
				</table>
				<button class = "btn btn-default" @click.prevent = "save()" >Send</button>
			</div>
			<div class = "step4 text-center" v-if = "step == 4">
				<button class = "alert alert-success" v-if = "quote_saved.total">Your quote has been saved! Total: $<span v-text = "quote_saved.total" ></span></button>
				
			</div>
			<div v-if = "error.length" class = "alert alert-danger">
					{{ error }}
				</div>
			<div class="text-center">
				<button class = "btn btn-default " type="submit" :disabled = "!isValidStep()" v-if = "step < 3" @click = "next()" >Next step</button>
			</div>
	</div>

	<script src="https://cdn.jsdelivr.net/npm/vue@2.5.17/dist/vue.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js"></script>

	<script type="text/javascript">

		var products = <?= ($products->toJson()) ?>;
		
	</script>

	<script type="text/javascript" src = "assets/js/app.js"></script>
</body>
</html>