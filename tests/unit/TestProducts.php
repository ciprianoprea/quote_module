<?php

use PHPUnit\Framework\TestCase;
use QuoteApp\Repositories\Product\ServiceRepository;
use QuoteApp\Repositories\Product\SubscriptionRepository;
use QuoteApp\Repositories\Product\GoodRepository;
use Carbon\Carbon;

class TestProducts extends TestCase
{

	public function test_service_cost()
	{
		$service = (new ServiceRepository())
			->interval(Carbon::parse('10.10.2018'),Carbon::parse('20.10.2018'))
			->setPrice(2);

		$this->assertEquals(180,$service->cost());

	}

	public function test_subscription_cost()
	{
		$subscription = (new SubscriptionRepository())
			->interval(Carbon::parse('10.10.2018'),Carbon::parse('20.10.2018'))
			->setPrice(10);

		$this->assertEquals(80,$subscription->cost());

	}

	public function test_good_cost()
	{
		$good = (new GoodRepository())
			->quantity(10)
			->setPrice(10);

		$this->assertEquals(100,$good->cost());

	}
}