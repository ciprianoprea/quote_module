<?php

use PHPUnit\Framework\TestCase;
use QuoteApp\Repositories\Product\ServiceRepository;
use QuoteApp\Repositories\Product\SubscriptionRepository;
use QuoteApp\Repositories\Product\GoodRepository;
use QuoteApp\Repositories\Sales\Quote;
use Carbon\Carbon;

require './bootstrap.php';
require './application/helpers.php';

class TestQuote extends TestCase
{

	public function test_quote_cost()
	{
		$request = [
			'user_details' => [
				'name' => 'Ciprian',
				'password' => 'secret',
				'email' => 'ciprian.oprea.home@gmail.com',
				'phone' => '0724900061'
			]
		];

		$request = (object) $request;

		$quote = new Quote();
		$quote->setUserDetails($request->user_details);
		$quote->addProduct(
			(new GoodRepository())
			->quantity(5)
			->setPrice(25)
			->setProductId(1)
			->setName('PHP Book')
		);

		$this->assertEquals(125,$quote->getTotalCost());

		$quote->addProduct(
			(new SubscriptionRepository())
			->interval(Carbon::parse("01.10.2018"),Carbon::parse("10.10.2018"))
			->setPrice(10)
			->setProductId(3)
			->setName('Test Subscription')
		);

		$this->assertEquals(125 + 70,$quote->getTotalCost());

		$quote->addProduct(
			(new ServiceRepository())
			->interval(Carbon::parse("01.10.2018"),Carbon::parse("10.10.2018"))
			->setPrice(2)
			->setProductId(2)
			->setName('Test Service')
		);

		$this->assertEquals(125 + 70 + 160,$quote->getTotalCost());

		$quoteResult = [
			'user_details' => [
				'name' => 'Ciprian',
				'password' => 'secret',
				'email' => 'ciprian.oprea.home@gmail.com',
				'phone' => '0724900061'
			],
			'products' => [
				[
					'name' => 'PHP Book',
					'type' => 'Good',
					'price' => 25,
					'quantity' => 5,
					'subtotal' => 125,
				],
				[
					'name' => 'Test Subscription',
					'type' => 'Subscription',
					'dailyPrice' => 10,
					'quantity' => 7,
					'subtotal' => 70,
				],
				[
					'name' =>'Test Service',
					'type' => 'Service',
					'hourlyPrice' => 2,
					'quantity' => 80,
					'subtotal' => 160,
				]
			],
			'total' => 355
		];

		$this->assertSame($quoteResult['products'] ,$quote->get()['products']);
		$quote->save();

		$validPassword = password_verify($request->user_details['password'],$quote->getUser()->password);

		$this->assertEquals($validPassword ,true);
		$this->assertEquals($request->user_details['phone'] ,$quote->getUser()->phone);



	}
	
}