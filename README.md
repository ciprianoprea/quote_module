## Introduction

This application is a 3-step quotation process

1. Let the user enter their details
2. Add products to quote. (There are three types of product (Service, Subscription, Goods)).
3. Send quote.


Requirements

PHP >= 7.1
MySql >= 5.x

Application utilizes [Composer](http://getcomposer.org/) to manage its dependencies. So, make sure you have Composer installed on your machine.


## Installation & Configuration

### Install dependencies

```sh
 composer install
```

### Create your database and set database configuration file.

```php

<?php

// config/database.php

return [

   "driver" => "mysql",

   "host" =>"localhost",

   "database" => "quote_db",

   "username" => "root",

   "password" => ""

];

```
### Create tables and seed db with few products


```sh
php seed.php
```

### Serving Your Application

```sh
php -S localhost:8000 -t public

```
Now you can visit http://localhost:8000 and it's ready to use.
