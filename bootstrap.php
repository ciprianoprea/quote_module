<?php 

use Illuminate\Database\Capsule\Manager as DB;

require 'vendor/autoload.php';

$database_config = require 'config/database.php';

$db = new DB;

$db->addConnection($database_config);
$db->setAsGlobal();
$db->bootEloquent();
